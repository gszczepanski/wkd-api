# wkd-api
API for fetching WKD timetable.

## What it is ##

This is simple API for obtaining Warszawska Kolej Dojazdowa train schedules. 
By wkd-api you can obtain next transits based on few parameters (Station from - to, time, date).

## How to use ##

```java

/**
 * Create WKD Schedule object
 */
WkdScheduleFactory factory = new WkdScheduleFactory();
WkdSchedule wkdSchedule = factory.create();

/**
 * Obtain nest transit from Pruszkow WKD to Warszawa Reduta Ordona
 */
WkdTransit wkdTransit = wkdSchedule.obtainNext(Station.PRUSZKOW_WKD, Station.WWA_REDUTA_ORDONA);

/**
 * Check some details
 */
LocalDateTime startArrivalDateTime = wkdTransit.getStartArrivalDateTime();
LocalDateTime destinationArrivalDateTime = wkdTransit.getDestinationArrivalDateTime();
int travelTimeInMinutes = wkdTransit.getTravelTimeInMinutes();
int distanceInKilometers = wkdTransit.getDistanceInKilometers();
List<StationTime> route = wkdTransit.getRoute();
```

## Dependencies ##

To use wkd-api you need to have following dependencies:
* Unirest v.1.4.9
* Guava v.22.0
* GSON v.2.8.2
