package szczepanski.gerard.wkdapi.core;

import java.time.LocalDate;
import java.time.LocalTime;

import static szczepanski.gerard.wkdapi.core.Station.PRUSZKOW_WKD;
import static szczepanski.gerard.wkdapi.core.Station.WWA_ALEJE_JEROZOLIMSKIE;

class FilterProvider {

    static TransitFilter filter() {
        return TransitFilter.builder()
                .start(PRUSZKOW_WKD)
                .destination(WWA_ALEJE_JEROZOLIMSKIE)
                .date(LocalDate.now())
                .from(LocalTime.of(12, 00, 00))
                .build();
    }

}
