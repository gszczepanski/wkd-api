package szczepanski.gerard.wkdapi.core;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.*;

public class StationTest {

    @Test
    public void shouldReturnFullWkdRouteSorted() {
        List<Station> stations = Station.fullWkdRoute();

        assertThat(stations.get(0)).isEqualTo(GRODZISK_MAZ_RADONSKA);
        assertThat(stations.get(13)).isEqualTo(PRUSZKOW_WKD);
        assertThat(stations.get(25)).isEqualTo(WWA_SRODMIESCIE_WKD);
    }


}