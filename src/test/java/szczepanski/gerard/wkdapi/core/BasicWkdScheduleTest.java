package szczepanski.gerard.wkdapi.core;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static szczepanski.gerard.wkdapi.core.Station.BRZOZKI;

public class BasicWkdScheduleTest {

    BasicWkdSchedule wkdSchedule;
    WkdTransits transits;

    @Before
    public void before() {
        transits = mock(WkdTransits.class);
        wkdSchedule = new BasicWkdSchedule(transits);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionItFilterPassedToTransitIsNull() {
        wkdSchedule.transits(null);
    }

    @Test
    public void shouldSearchForTransitsWithFilter() {
        TransitFilter filter = FilterProvider.filter();
        wkdSchedule.transits(filter);

        verify(transits).searchForTransits(filter);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenStartStationIsNullnObtainNext() {
        wkdSchedule.obtainNext(null, BRZOZKI);
    }


    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenDestinationStationIsNullnObtainNext() {
        wkdSchedule.obtainNext(BRZOZKI, null);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenStartStationITheSameAsDestinationStation() {
        wkdSchedule.obtainNext(BRZOZKI, BRZOZKI);
    }

}