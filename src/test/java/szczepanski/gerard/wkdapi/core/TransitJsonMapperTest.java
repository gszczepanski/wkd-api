package szczepanski.gerard.wkdapi.core;

import org.junit.Test;
import szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonRoute;
import szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonStation;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdServerResponseBody;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonSchedule;
import static szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonScheduleAdditionalInfo;

public class TransitJsonMapperTest {

    @Test
    public void shouldReturnJsonScheduleWithoutRoutesWhenResponseIsNotValidJson() {
        TransitJsonMapper mapper = new TransitJsonMapper();
        JsonSchedule jsonSchedule = mapper.convertToJsonSchedule(WkdServerResponseBody.of("not valid json"));

        assertThat(jsonSchedule.isEmpty()).isEqualTo(true);
    }

    @Test
    public void shouldReturnJsonScheduleWithOneRoute() {
        TransitJsonMapper mapper = new TransitJsonMapper();
        JsonSchedule jsonSchedule = mapper.convertToJsonSchedule(serverResponse());

        assertThat(jsonSchedule.getRoute().size()).isEqualTo(1);
    }

    @Test
    public void shouldReturnJsonScheduleWithValidAdditionalInfo() {
        TransitJsonMapper mapper = new TransitJsonMapper();
        JsonSchedule jsonSchedule = mapper.convertToJsonSchedule(serverResponse());
        JsonScheduleAdditionalInfo info = jsonSchedule.getInfo();

        assertThat(info.getDistance()).isEqualTo("11");
        assertThat(info.getTime()).isEqualTo("00:18");
    }

    @Test
    public void shouldReturnJsonScheduleWithValidRoute() {
        TransitJsonMapper mapper = new TransitJsonMapper();
        JsonSchedule jsonSchedule = mapper.convertToJsonSchedule(serverResponse());
        JsonRoute jsonRoute = jsonSchedule.getRoute().get(0);

        assertThat(jsonRoute.getArr()).isEqualTo("12:19");
        assertThat(jsonRoute.getServiceType()).isEqualTo("WKD");
        assertThat(jsonRoute.getSymbol()).isEqualTo("A1");
        assertThat(jsonRoute.getLow_floor()).isEqualTo(1);
        assertThat(jsonRoute.getIntermediateStations().size()).isEqualTo(2);
    }

    @Test
    public void shouldReturnJsonScheduleWithValidStations() {
        TransitJsonMapper mapper = new TransitJsonMapper();
        JsonSchedule jsonSchedule = mapper.convertToJsonSchedule(serverResponse());
        JsonRoute jsonRoute = jsonSchedule.getRoute().get(0);
        JsonStation jsonStation = jsonRoute.getIntermediateStations().get(0);

        assertThat(jsonStation.getArr()).isEqualTo("");
        assertThat(jsonStation.getName()).isEqualTo("Milanówek Grudów");
        assertThat(jsonStation.getDep()).isEqualTo("11:58");
    }

    private WkdServerResponseBody serverResponse() {
        return WkdServerResponseBody.of(json());
    }

    private String json() {
        return "{\"route\":[{\"arr\":\"12:19\",\"serviceType\":\"WKD\",\"symbol\":\"A1\",\"low_floor\":1,\"intermediateStations\":[{\"arr\":\"\",\"serviceType\":\"WKD\",\"name\":\"Milanówek Grudów\",\"active\":0,\"dep\":\"11:58\"},{\"arr\":\"12:01\",\"serviceType\":\"WKD\",\"name\":\"Polesie\",\"active\":0,\"dep\":\"12:01\"}],\"time\":\"00:18\",\"dayOperationCode\":\"C\",\"dep\":\"12:37\"}],\"info\":{\"distance\":11,\"time\":\"00:18\"}}";
    }
}