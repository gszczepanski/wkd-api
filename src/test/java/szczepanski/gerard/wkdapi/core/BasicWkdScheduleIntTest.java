package szczepanski.gerard.wkdapi.core;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.PRUSZKOW_WKD;
import static szczepanski.gerard.wkdapi.core.Station.WWA_ALEJE_JEROZOLIMSKIE;

public class BasicWkdScheduleIntTest {

    @Test
    public void shouldObtainNextTransit() {
        WkdScheduleFactory factory = new WkdScheduleFactory();
        WkdSchedule wkdSchedule = factory.create();
        WkdTransit next = wkdSchedule.obtainNext(PRUSZKOW_WKD, WWA_ALEJE_JEROZOLIMSKIE);

        assertThat(next).isNotNull();
    }

    @Test
    public void shouldObtainTransitsByFilter() {
        WkdScheduleFactory factory = new WkdScheduleFactory();
        WkdSchedule wkdSchedule = factory.create();
        List<WkdTransit> transits = wkdSchedule.transits(FilterProvider.filter());

        assertThat(transits).isNotEmpty();
    }

    @Test
    public void shouldHandleNoNextTransitsToday() {
        WkdScheduleFactory factory = new WkdScheduleFactory();
        WkdSchedule wkdSchedule = factory.create();

        List<WkdTransit> transits = wkdSchedule.transits(TransitFilter.builder()
                .start(PRUSZKOW_WKD)
                .destination(WWA_ALEJE_JEROZOLIMSKIE)
                .date(LocalDate.now())
                .from(LocalTime.of(22, 55, 00))
                .build());

        assertThat(transits).isNotEmpty();
    }

}
