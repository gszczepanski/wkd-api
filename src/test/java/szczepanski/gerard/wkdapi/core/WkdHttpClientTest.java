package szczepanski.gerard.wkdapi.core;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams;

import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.KAZIMIEROWKA;
import static szczepanski.gerard.wkdapi.core.Station.OPACZ;
import static szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams.builder;

public class WkdHttpClientTest {

    @ClassRule
    public static WireMockClassRule wkdEndpoint = new WireMockClassRule(9999);

    WkdHttpClient client;

    String ENDPOINT_PATH = "/schedule";
    String ENDPOINT_URL = "http://localhost:9999" + ENDPOINT_PATH;

    @Before
    public void setUp() throws Exception {
        client = new WkdHttpClient(new URL(ENDPOINT_URL));
    }

    @Test
    public void shouldReturnJsonWhenResponseIsOk() throws Exception {
        String JSON = "{\"schedule\":\"OK\"}";
        stubFor(post(urlEqualTo(ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBody(JSON)
                ));

        assertThat(client.fetchWkdTransitsFromServer(params()).getResponse()).isEqualTo(JSON);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenParamsPassedAreNull() throws Exception {
        client.fetchWkdTransitsFromServer(null);
    }

    @Test(expected = MalformedURLException.class)
    public void shouldThrowExceptionWhenUrlIsInvalid() throws Exception {
        client = new WkdHttpClient(new URL("invalidURL"));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEWhenUrlIsNull() throws Exception {
        client = new WkdHttpClient(null);
    }

    @Test
    public void shouldReturnResponseWithNotValidJsonWhenWhenServerResponseWith500() {
        stubFor(post(urlEqualTo(ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withBody("Internal server error")
                        .withHeader("Content-Type", "text/html")
                ));

        assertThat(client.fetchWkdTransitsFromServer(params()).isValidJson()).isFalse();
    }

    @Test(expected = WkdApiException.class)
    public void shouldThrowWkdApiExceptionOnSocketTimeOut() {
        stubFor(post(urlEqualTo(ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withFixedDelay(2100)
                ));

        client.fetchWkdTransitsFromServer(params());
    }

    @Test
    public void shouldReturnResponseWithNotValidJsonWhenBodyReturnedFromServerIsHtml() {
        stubFor(post(urlEqualTo(ENDPOINT_PATH))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("<html>No transits buddy</html>")
                        .withHeader("Content-Type", "text/html")
                ));

        assertThat(client.fetchWkdTransitsFromServer(params()).isValidJson()).isFalse();
    }

    private WkdHttpParams params() {
        return builder()
                .startStation(KAZIMIEROWKA.getStationName())
                .destinationStation(OPACZ.getStationName())
                .date("2018-02-11")
                .timeFrom("12:00")
                .build();
    }
}