package szczepanski.gerard.wkdapi.core;

import org.junit.Test;
import szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonSchedule;

import static org.assertj.core.api.Assertions.assertThat;

public class WkdTransitFactoryTest {

    @Test
    public void shouldReturnEmptyListWhenScheduleIsEmpty() {
        WkdTransitFactory factory = new WkdTransitFactory();
        JsonSchedule schedule = new JsonSchedule();

        assertThat(factory.create(schedule, FilterProvider.filter())).isEmpty();
    }

}
