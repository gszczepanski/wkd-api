package szczepanski.gerard.wkdapi.core;

import org.junit.Test;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdServerResponseBody;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.KAZIMIEROWKA;
import static szczepanski.gerard.wkdapi.core.Station.OPACZ;
import static szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams.builder;

public class WkdHttpClientIntTest {

    @Test
    public void shouldFetchJsonFromRemote() throws Exception {
        WkdHttpParams params = builder()
                .startStation(KAZIMIEROWKA.getStationName())
                .destinationStation(OPACZ.getStationName())
                .date("2018-01-21")
                .timeFrom("12:00")
                .build();
        WkdHttpClient client = new WkdHttpClient(new URL("http://wkd.com.pl/rozklad/rozklad.php"));

        WkdServerResponseBody serverResponseBody = client.fetchWkdTransitsFromServer(params);

        assertThat(serverResponseBody.getResponse()).contains("\"name\":\"Pruszk\\u00f3w WKD\"");
    }
}