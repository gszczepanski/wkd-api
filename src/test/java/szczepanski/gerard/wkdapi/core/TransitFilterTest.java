package szczepanski.gerard.wkdapi.core;

import org.junit.Test;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.KOMOROW;
import static szczepanski.gerard.wkdapi.core.Station.PODKOWA_LESNA_WSCHODNIA;

public class TransitFilterTest {

    @Test
    public void shouldCreateWkdHttpParamsObject() {
        TransitFilter filter = filter();

        assertThat(filter.toParams()).isNotNull();
    }

    @Test
    public void shouldCreatedWkdParamsStartStationParamBeStationName() {
        TransitFilter filter = filter();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getStartStation()).isEqualTo(KOMOROW.getStationName());
    }

    @Test
    public void shouldCreatedWkdParamsDestinationStationParamBeStationName() {
        TransitFilter filter = filter();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getDestinationStation()).isEqualTo(PODKOWA_LESNA_WSCHODNIA.getStationName());
    }

    @Test
    public void shouldCreatedWkdParamsDateHasValidFormat() {
        TransitFilter filter = filter();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getDate()).isEqualTo("2018-01-12");
    }

    @Test
    public void shouldCreatedWkdParamsTimeFromHasValidFormat() {
        TransitFilter filter = filter();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getTimeFrom()).isEqualTo("12:15");
    }

    @Test
    public void shouldCreatedWkdParamsTimeToBeNullIfNotSpecified() {
        TransitFilter filter = filter();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getTimeTo()).isNull();
    }

    @Test
    public void shouldCreatedWkdParamsTimeToHasValidFormat() {
        TransitFilter filter = filterWithTimeTo();
        WkdHttpParams params = filter.toParams();

        assertThat(params.getTimeTo()).isEqualTo("14:30");
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenStartStationIsNull() {
        TransitFilter.builder()
                .start(null)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(12, 15, 00))
                .build();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenDestinationStationIsNull() {
        TransitFilter.builder()
                .start(KOMOROW)
                .destination(null)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(12, 15, 00))
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenDestinationStationIsthaSameAsStartStation() {
        TransitFilter.builder()
                .start(KOMOROW)
                .destination(KOMOROW)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(12, 15, 00))
                .build();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenDateIsNull() {
        TransitFilter.builder()
                .start(KOMOROW)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(null)
                .from(LocalTime.of(12, 15, 00))
                .build();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenTimeFromIsNull() {
        TransitFilter.builder()
                .start(KOMOROW)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(LocalDate.of(2018, 01, 12))
                .from(null)
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenTimeFromIsTheSameAsTimeTo() {
        TransitFilter.builder()
                .start(KOMOROW)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(14, 30, 00))
                .to(LocalTime.of(14, 30, 00))
                .build();
    }

    private TransitFilter filter() {
        return TransitFilter.builder()
                .start(KOMOROW)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(12, 15, 00))
                .build();
    }

    private TransitFilter filterWithTimeTo() {
        return TransitFilter.builder()
                .start(KOMOROW)
                .destination(PODKOWA_LESNA_WSCHODNIA)
                .date(LocalDate.of(2018, 01, 12))
                .from(LocalTime.of(12, 15, 00))
                .to(LocalTime.of(14, 30, 00))
                .build();
    }
}