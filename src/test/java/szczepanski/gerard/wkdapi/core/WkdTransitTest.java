package szczepanski.gerard.wkdapi.core;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;
import static szczepanski.gerard.wkdapi.core.Station.*;

public class WkdTransitTest {

    @Test
    public void shouldReturnValidDistanceInKilometers() {
        int distanceInKilometers = 12;
        WkdTransit transit = new WkdTransit(distanceInKilometers, 2, route());

        assertThat(transit.getDistanceInKilometers()).isEqualTo(distanceInKilometers);
    }

    @Test
    public void shouldReturnValidTravelTimeInMinutes() {
        int travelTime = 25;
        WkdTransit transit = new WkdTransit(2, travelTime, route());

        assertThat(transit.getTravelTimeInMinutes()).isEqualTo(travelTime);
    }

    @Test
    public void shouldReturnStartStationOfRoute() {
        WkdTransit transit = new WkdTransit(2, 2, route());

        assertThat(transit.getStartStation().getStation()).isEqualTo(KOMOROW);
    }

    @Test
    public void shouldReturnDestinationStationOfRoute() {
        WkdTransit transit = new WkdTransit(2, 2, route());

        assertThat(transit.getDestinationStation().getStation()).isEqualTo(MALICHY);
    }

    @Test
    public void shouldReturnRouteOfTransit() {
        WkdTransit transit = new WkdTransit(2, 2, route());

        assertThat(transit.getRoute().size()).isEqualTo(route().size());
        ;
    }

    private LinkedList<StationTime> route() {
        LinkedList<StationTime> route = new LinkedList<>();

        route.add(new StationTime(KOMOROW, LocalDateTime.now()));
        route.add(new StationTime(PRUSZKOW_WKD, LocalDateTime.now()));
        route.add(new StationTime(TWORKI, LocalDateTime.now()));
        route.add(new StationTime(MALICHY, LocalDateTime.now()));

        return route;
    }
}