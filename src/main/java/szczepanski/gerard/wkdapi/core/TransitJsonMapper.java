package szczepanski.gerard.wkdapi.core;

import com.google.gson.Gson;
import lombok.Getter;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdServerResponseBody;

import java.util.ArrayList;
import java.util.List;

class TransitJsonMapper {

    JsonSchedule convertToJsonSchedule(WkdServerResponseBody response) {
        if (!response.isValidJson()) {
            return new JsonSchedule();
        }
        return transformJsonSchedule(response.getResponse());
    }

    private JsonSchedule transformJsonSchedule(String json) {
        return new Gson().fromJson(json, JsonSchedule.class);
    }

    @Getter
    static class JsonSchedule {
        private List<JsonRoute> route = new ArrayList<>(0);
        private JsonScheduleAdditionalInfo info = new JsonScheduleAdditionalInfo();

        boolean isEmpty() {
            return route.isEmpty();
        }

    }

    @Getter
    static class JsonScheduleAdditionalInfo {

        private String distance = "";
        private String time = "";

    }

    @Getter
    static class JsonRoute {

        private String arr;
        private String symbol;
        private Integer low_floor;
        private String serviceType;
        private String time;
        private String dayOperationCode;
        private String dep;
        private List<JsonStation> intermediateStations;

    }

    @Getter
    static class JsonStation {

        private String arr;
        private String dep;
        private String name;

    }

}
