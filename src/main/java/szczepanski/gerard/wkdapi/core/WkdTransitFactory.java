package szczepanski.gerard.wkdapi.core;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Integer.valueOf;
import static java.util.stream.Collectors.toList;
import static szczepanski.gerard.wkdapi.core.TransitJsonMapper.*;

class WkdTransitFactory {


    List<WkdTransit> create(JsonSchedule schedule, TransitFilter filter) {
        if (schedule.isEmpty()) {
            return new ArrayList<>();
        }
        return transform(schedule, filter);
    }

    private List<WkdTransit> transform(JsonSchedule schedule, TransitFilter filter) {
        int distance = distanceInKilometers(schedule);

        return schedule.getRoute()
                .stream()
                .map(r -> jsonRouteToTransit(r, distance, filter))
                .collect(toList());
    }

    private int distanceInKilometers(JsonSchedule schedule) {
        String distance = schedule.getInfo().getDistance();
        return valueOf(distance);
    }

    private WkdTransit jsonRouteToTransit(JsonRoute route, int distance, TransitFilter filter) {
        int time = timeInMinutes(route);

        LinkedList<StationTime> stations = new LinkedList<>();
        List<JsonStation> jsonStations = transitStations(route, filter);

        jsonStations.forEach(s -> stations.add(toStationTime(s, filter.getDate())));

        return new WkdTransit(distance, time, stations);
    }

    private int timeInMinutes(JsonRoute route) {
        String time = route.getTime();
        String[] hoursAndMinutes = time.split(":");

        int minutes = valueOf(hoursAndMinutes[1]);
        minutes += valueOf(hoursAndMinutes[0]) * 60;
        return minutes;
    }

    private List<JsonStation> transitStations(JsonRoute route, TransitFilter filter) {
        List<JsonStation> intermediateStations = route.getIntermediateStations();

        JsonStation start = intermediateStations.stream().filter(s -> s.getName().equals(filter.getStart().getStationName())).findFirst().orElseThrow(() -> new RuntimeException("Could not find start station"));
        JsonStation destination = intermediateStations.stream().filter(s -> s.getName().equals(filter.getDestination().getStationName())).findFirst().orElseThrow(() -> new RuntimeException("Could not find destination station"));
        return intermediateStations.subList(intermediateStations.indexOf(start), intermediateStations.indexOf(destination) + 1);
    }

    private StationTime toStationTime(JsonStation station, LocalDate date) {
        String time = station.getArr().equals("") ? station.getDep() : station.getArr();
        LocalTime parsedTime = LocalTime.parse(time);
        return new StationTime(Station.fromStationName(station.getName()), LocalDateTime.of(date, parsedTime));
    }
}
