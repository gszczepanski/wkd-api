package szczepanski.gerard.wkdapi.core;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;


@RequiredArgsConstructor
class BasicWkdSchedule implements WkdSchedule {

    private final WkdTransits transits;

    @Override
    public WkdTransit obtainNext(Station start, Station destination) {
        checkNotNull(start, "Start station must not be null!");
        checkNotNull(destination, "Destination station must not be null!");
        Preconditions.checkState(start != destination, "Stations must not be the same");

        TransitFilter filter = TransitFilter.builder()
                .start(start)
                .destination(destination)
                .date(LocalDate.now())
                .from(LocalTime.now())
                .build();

        return transits.searchForTransits(filter).get(0);
    }

    @Override
    public List<WkdTransit> transits(TransitFilter filter) {
        checkNotNull(filter, "Filter must not be null!");
        return transits.searchForTransits(filter);
    }

}
