package szczepanski.gerard.wkdapi.core;

import lombok.RequiredArgsConstructor;

import java.util.List;

import static szczepanski.gerard.wkdapi.core.TransitJsonMapper.JsonSchedule;
import static szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdServerResponseBody;

@RequiredArgsConstructor
class WkdTransits {

    private final WkdHttpClient httpClient;
    private final TransitJsonMapper jsonMapper;
    private final WkdTransitFactory wkdTransitFactory;

    List<WkdTransit> searchForTransits(TransitFilter filter) {
        WkdServerResponseBody response = httpClient.fetchWkdTransitsFromServer(filter.toParams());
        JsonSchedule jsonSchedule = jsonMapper.convertToJsonSchedule(response);
        return wkdTransitFactory.create(jsonSchedule, filter);
    }

}
