package szczepanski.gerard.wkdapi.core;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import szczepanski.gerard.wkdapi.core.WkdHttpClient.WkdHttpParams;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

@Value
@EqualsAndHashCode
@Builder
public class TransitFilter {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    private final Station start;
    private final Station destination;
    private final LocalDate date;
    private final LocalTime from;
    private final LocalTime to;

    private TransitFilter(Station start, Station destination, LocalDate date, LocalTime from, LocalTime to) {
        validateParams(start, destination, date, from, to);

        this.start = start;
        this.destination = destination;
        this.date = date;
        this.from = from;
        this.to = to;
    }

    private void validateParams(Station start, Station destination, LocalDate date, LocalTime from, LocalTime to) {
        checkNotNull(start, "Start station must not be null!");
        checkNotNull(destination, "Destination station must not be null!");
        checkNotNull(date, "Date must not be null!");
        checkNotNull(from, "Time from must not be null!");

        checkState(start != destination, "Stations must not be the same");

        if (to != null) {
            checkState(from.isBefore(to), "time from must be before time to!");
        }
    }

    WkdHttpParams toParams() {
        return WkdHttpParams.builder()
                .startStation(start.getStationName())
                .destinationStation(destination.getStationName())
                .date(DATE_FORMATTER.format(date))
                .timeFrom(TIME_FORMATTER.format(from))
                .timeTo(to == null ? null : TIME_FORMATTER.format(to))
                .build();

    }

}

