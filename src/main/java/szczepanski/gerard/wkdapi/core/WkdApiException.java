package szczepanski.gerard.wkdapi.core;

public class WkdApiException extends RuntimeException {

    public WkdApiException(String message, Throwable cause) {
        super(message, cause);
    }
}