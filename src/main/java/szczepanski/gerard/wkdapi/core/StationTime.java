package szczepanski.gerard.wkdapi.core;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;

import static lombok.AccessLevel.MODULE;

@Value
@RequiredArgsConstructor(access = MODULE)
public class StationTime {

    private final Station station;
    private final LocalDateTime dateTime;

}
