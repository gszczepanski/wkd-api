package szczepanski.gerard.wkdapi.core;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonMap;

class WkdHttpClient {

    private final URL wkdServerEndpointURL;

    WkdHttpClient(URL wkdServerEndpointURL) {
        checkNotNull(wkdServerEndpointURL, "Error during construction of WkdHttpClient. URL can not be null!");

        this.wkdServerEndpointURL = wkdServerEndpointURL;
        Unirest.setTimeouts(1000, 2000);
    }

    public WkdServerResponseBody fetchWkdTransitsFromServer(WkdHttpParams params) {
        checkNotNull(params, "Given WkdHttpParams can not be null!");

        try {
            return WkdServerResponseBody.of(fetchTransitsResponseFromServer(params));
        } catch (UnirestException e) {
            throw new WkdApiException("Error with fetching data from WKD server", e);
        }
    }

    private String fetchTransitsResponseFromServer(WkdHttpParams params) throws UnirestException {
        HttpResponse<String> response = Unirest.post(wkdServerEndpointURL.toString())
                .headers(headers())
                .fields(fields(params))
                .asString();

        return response.getBody().toString();
    }

    private Map<String, String> headers() {
        return singletonMap("Content-Type", "application/x-www-form-urlencoded");
    }

    private Map<String, Object> fields(WkdHttpParams params) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("base", params.getStartStation());
        fields.put("target", params.getDestinationStation());
        fields.put("date", params.getDate());
        fields.put("from", params.getTimeFrom());
        fields.put("to", params.getTimeTo());
        return fields;
    }

    @Builder
    @Value
    static class WkdHttpParams {

        private final String startStation;
        private final String destinationStation;
        private final String date;
        private final String timeFrom;
        private final String timeTo;

    }

    @Value
    @RequiredArgsConstructor(staticName = "of")
    static class WkdServerResponseBody {

        private final String response;

        boolean isValidJson() {
            if (response == null) {
                return false;
            }
            return determineIsJson();
        }

        private boolean determineIsJson() {
            try {
                new Gson().fromJson(response, Object.class);
                return true;
            } catch (com.google.gson.JsonSyntaxException ex) {
                return false;
            }
        }
    }

}
