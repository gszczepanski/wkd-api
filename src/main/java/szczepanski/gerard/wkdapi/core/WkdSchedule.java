package szczepanski.gerard.wkdapi.core;

import java.util.List;

public interface WkdSchedule {

    /**
     * Get next WkdTransit from start station, to destination station.
     *
     * @param start       - Start station
     * @param destination - Destination station
     * @return WkdTransit object from next route.
     */
    WkdTransit obtainNext(Station start, Station destination);

    /**
     * Get list of WkdTransit which meets the TransitFilter object criteria.
     * @param filter - TransitFilter object.
     * @return List of WkdTransit objects.
     */
    List<WkdTransit> transits(TransitFilter filter);

}
