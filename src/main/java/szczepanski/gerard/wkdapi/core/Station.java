package szczepanski.gerard.wkdapi.core;

import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

@Getter
@RequiredArgsConstructor
public enum Station {

    WWA_SRODMIESCIE_WKD("W-wa Śródmieście WKD", 26),
    WWA_OCHOTA_WKD("W-wa Ochota WKD", 25),
    WWA_ZACHODNIA_WKD("W-wa Zachodnia WKD", 24),
    WWA_REDUTA_ORDONA("W-wa Reduta Ordona", 23),
    WWA_ALEJE_JEROZOLIMSKIE("W-wa Aleje Jerozolimskie", 22),
    WWA_RAKOW("W-wa Raków", 21),
    WWA_SALOMEA("W-wa Salomea", 20),
    OPACZ("Opacz", 19),
    MICHALOWICE("Michałowice", 18),
    REGULY("Reguły", 17),
    MALICHY("Malichy", 16),
    TWORKI("Tworki", 15),
    PRUSZKOW_WKD("Pruszków WKD", 14),
    KOMOROW("Komorów", 13),
    NOWA_WIES_WARSZAWSKA("Nowa Wieś Warszawska", 12),
    KANIE_HELENOWSKIE("Kanie Helenowskie", 11),
    OTEBUSY("Otrębusy", 10),
    PODKOWA_LESNA_WSCHODNIA("Podkowa Leśna Wschodnia", 9),
    PODKOWA_LESNA_GLOWNA("Podkowa Leśna Główna", 8),
    PODKOWA_LESNA_ZACHODNIA("Podkowa Leśna Zachodnia", 7),
    KAZIMIEROWKA("Kazimierówka", 6),
    BRZOZKI("Brzózki", 5),
    GRODZISK_MAZ_OKREZNA("Grodzisk Maz. Okrężna", 4),
    GRODZISK_MAZ_PIASKOWA("Grodzisk Maz. Piaskowa", 3),
    GRODZISK_MAZ_JORDANOWICE("Grodzisk Maz. Jordanowice", 2),
    GRODZISK_MAZ_RADONSKA("Grodzisk Maz. Radońska", 1);

    private final String stationName;
    private final Integer number;

    public static List<Station> fullWkdRoute() {
        return Stream.of(Station.values())
                .sorted(Comparator.comparing(Station::getNumber))
                .collect(toList());
    }

    public static Station fromStationName(String stationName) {
        checkNotNull(stationName, "Station name must not be null!");

        return fullWkdRoute()
                    .stream()
                        .filter(s -> s.getStationName().equals(stationName))
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException("Can not convert given station name to Station!"));

    }

}
