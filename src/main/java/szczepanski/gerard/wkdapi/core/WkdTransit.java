package szczepanski.gerard.wkdapi.core;

import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static lombok.AccessLevel.PACKAGE;

/**
 * Represents one WKD Transit.
 * <br><br>
 * Contains information about requested start station, destination station, distance in kilometers, travel time and intermediate stations (route).
 */

@RequiredArgsConstructor(access = PACKAGE)
public class WkdTransit {

    private final int distanceInKilometers;
    private final int travelTimeInMinutes;
    private final LinkedList<StationTime> route;

    public StationTime getStartStation() {
        return route.getFirst();
    }

    public StationTime getDestinationStation() {
        return route.getLast();
    }

    public List<StationTime> getRoute() {
        return Collections.unmodifiableList(route);
    }

    public int getDistanceInKilometers() {
        return distanceInKilometers;
    }

    public int getTravelTimeInMinutes() {
        return travelTimeInMinutes;
    }

    public LocalDateTime getStartArrivalDateTime() {
        return getStartStation().getDateTime();
    }

    public LocalDateTime getDestinationArrivalDateTime() {
        return getDestinationStation().getDateTime();
    }
}
