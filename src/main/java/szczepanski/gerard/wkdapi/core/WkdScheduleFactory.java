package szczepanski.gerard.wkdapi.core;

import java.net.MalformedURLException;
import java.net.URL;

public class WkdScheduleFactory {

    private static final String ENDPOINT_URL = "http://wkd.com.pl/rozklad/rozklad.php";

    /**
     * Creates WkdSchedule object for obtaining WKD schedule.
     */
    public WkdSchedule create() {
        return new BasicWkdSchedule(wkdTransits());
    }

    private WkdTransits wkdTransits() {
        return new WkdTransits(wkdHttpClient(), transitJsonMapper(), wkdTransitFactory());
    }

    private WkdHttpClient wkdHttpClient() {
        return new WkdHttpClient(endpointURL());
    }

    private URL endpointURL() {
        try {
            return new URL(ENDPOINT_URL);
        } catch (MalformedURLException e) {
            throw new WkdApiException("Unable to set endpoint url. Please contact with author", e);
        }
    }

    private TransitJsonMapper transitJsonMapper() {
        return new TransitJsonMapper();
    }

    private WkdTransitFactory wkdTransitFactory() {
        return new WkdTransitFactory();
    }

}
